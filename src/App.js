import React, { Component } from "react";
import "./App.css";
import ReactPlayer from "react-player";

import "antd/dist/antd.css";

class App extends Component {
  state = {
    url: ""
  };

  // componentDidMount = () => {
  //   fetch(`${process.env.PUBLIC_URL}/config/config.json`)
  //     .then(response => response.json())
  //     .then(data => {
  //       this.setState({ url: data.urls[0].url });
  //       console.log(this.state.url);
  //     });
  // };

  render() {
    return (
      <div className="player-wrapper">
        <ReactPlayer
          className="react-player"
          url={process.env.REACT_APP_ORIGIN_URL}
          playing={true}
          controls={true}
          muted={true}
          width="50%"
          height="50%"
          // onPlay={() => console.log("onPlay")}
          // onReady={() => console.log("onReady")}
          // onStart={() => console.log("onStart")}
        />
      </div>
    );
  }
}

export default App;
